function tuyenSinh(){
    var diemChuan = document.getElementById("diem-chuan").value*1;
    var khuVuc = document.getElementById("khu-vuc").value*1;
    var doiTuong = document.getElementById("doi-tuong").value*1;
    var diem1 = document.getElementById("diem-mon-1").value*1;
    var diem2 = document.getElementById("diem-mon-2").value*1;
    var diem3 = document.getElementById("diem-mon-3").value*1;
    if(diem1*diem2*diem3 == 0){
        document.getElementById("result").innerHTML = `Xin chia buồn, bạn đã trượt tốt nghiệp`
    }
    else{
        var tongKet = diem1 + diem2 + diem3 + khuVuc + doiTuong;
        if(tongKet<diemChuan){
            document.getElementById("result").innerHTML = `Xin chia buồn, bạn đã trượt tốt nghiệp`
        }
        else{
            document.getElementById("result").innerHTML = `Chúc mừng, bạn đã đậu tốt nghiệp với số điểm tổng kết: ${tongKet}`
        }
    }
}

function tienDien(){
    var hoTen = document.getElementById("ho-va-ten").value;
    var soKW = document.getElementById("so-kw").value*1;
    var res = 0;
    if(soKW <= 50){
        res = soKW*500;
    }
    else if(soKW <= 100){
        res = 50*500 + (soKW-50)*650;
    }
    else if(soKW <= 200){
        res = 50*500 + 50*650 + (soKW-100)*850;
    }
    else if(soKW <= 350){
        res = 50*500 + 50*650 + 100*850 + (soKW-200)*1100;
    }
    else{
        res = 50*500 + 50*650 + 100*850 + 150*1100 + (soKW-350)*1300;
    }
    res = res.toLocaleString('vi-VN');
    document.getElementById("result").innerHTML = `Họ và tên: ${hoTen}; Tiền điện: ${res}`;
}

function tienThue(){
    var hoTen = document.getElementById("ho-va-ten").value;
    var namThuNhap = document.getElementById("thu-nhap").value*1;
    var nguoiPhuThuoc = document.getElementById("nguoi-phu-thuoc").value*1;
    var thuNhapChiuThue = namThuNhap - 4e6 - nguoiPhuThuoc*1.6*1e6;
    var Thue = 0;
    if(thuNhapChiuThue <= 60*1e6){
        Thue = thuNhapChiuThue*0.05;
        console.log(1);
    }
    else if(thuNhapChiuThue <= 120*1e6){
        Thue = 60*1e6*0.05 + (thuNhapChiuThue-60*1e6)*0.1; 
        console.log(2);
    }
    else if(thuNhapChiuThue <= 210*1e6){
        Thue = 60*1e6*0.05 + 60*1e6*0.1 + (thuNhapChiuThue-120*1e6)*0.15;
        console.log(3);
    }
    else if(thuNhapChiuThue <= 384*1e6){
        Thue = 60*1e6*0.05 + 60*1e6*0.1 + 90*1e6*0.15 + (thuNhapChiuThue-210*1e6)*0.25;
        console.log(4);
    }
    else if(thuNhapChiuThue <= 624*1e6){
        Thue = 60*1e6*0.05 + 60*1e6*0.1 + 90*1e6*0.15 + 174*1e6*0.25 + (thuNhapChiuThue-384*1e6)*0.3;
        console.log(5);
    }
    else{
        Thue = 60*1e6*0.05 + 60*1e6*0.1 + 90*1e6*0.15 + 174*1e6*0.25 + 240*1e6*0.3 + (thuNhapChiuThue-624*1e6)*0.35; 
        console.log(6);
    }
    if(Thue > 0){
        Thue = Thue.toLocaleString('vi-VN');
        document.getElementById("result").innerHTML = `Họ và tên: ${hoTen}; Tiền thuế thu nhập cá nhân: ${Thue}`;
    }
    else{
        alert("Số tiền thu nhập không hợp lệ");
    }
    
}

// Onchange
function myFunction(){
    var x = document.getElementById("khach-hang").value*1;
    if(x == 1) {
        document.getElementById("change").innerHTML = 
    `   <input type="text"
        class="form-control"  
        id="so-ket-noi" 
        placeholder="Số kết nối">
    `;
    }
    else {
        document.getElementById("change").innerHTML = ``;
    }
}
function tienCap(){
    var KH = document.getElementById("khach-hang").value*1;
    var maKH = document.getElementById("ma-KH").value;
    var kenhCC = document.getElementById("so-kenh").value*1;
    var tongTien = 0;
    if(KH == 0){
        tongTien = 4.5 + 20.5 + 7.5*kenhCC;
    }
    else{
        var soKetNoi = document.getElementById("so-ket-noi").value*1;
        var tienKetNoi = 0;
        if(soKetNoi <= 10){
            tienKetNoi = 75;
        }
        else{
            tienKetNoi = 75 + 5*(soKetNoi-10);
        }
        tongTien = 15 + tienKetNoi + 50*kenhCC;
    }
    tongTien = tongTien.toLocaleString('vi-VN');
    document.getElementById("result").innerHTML = `Mã khách hàng: ${maKH}; Tiền cáp: $${tongTien}`;
}